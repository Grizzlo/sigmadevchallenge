import React, { Component } from 'react';
import {Redirect} from "react-router-dom";
import SignPopup from "../../components/SignPopup";
import './signup.css'

//Component for Signup Page with field validators
class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            confirmPassword: '',
            formErrors: {email: '', password: '', confirmPassword: ''},
            emailValid: false,
            passwordValid: false,
            confirmPasswordValid: false,
            formValid: false,
            redirect: false,
            validClass: {email: 'valid',  password: 'valid', confirmPassword: 'valid'},
            showPopup:false,
            success:false,
            message:'',
        };
        this.changePopupState = this.changePopupState.bind(this);
    };

    changePopupState(){
        this.setState(prevState =>({
      showPopup: !prevState.showPopup
      }))
    }

     handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
            () => {this.validateField(name, value)}
        );
    };

     validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let confirmPasswordValid = this.state.confirmPasswordValid;
        let validClass = this.state.validClass;

        switch(fieldName) {
            case 'email':
                emailValid = value.match(/^(?=.{10,50}$)([a-z0-9_-]+.)*[a-z0-9_-]+@[a-z0-9_-]+(.[a-z0-9_-]+)*.[a-z]{2,6}$/i);
                fieldValidationErrors.email = emailValid ? '' : 'Enter correct email. Length should be from 10 to 50 chars';
                validClass.email = emailValid ? 'valid': 'invalid';
                break;
            case 'password':
                passwordValid = value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,50}$/);
                fieldValidationErrors.password = passwordValid ? '': '6-50 symbols, big letter, small letter, number, @#$%';
                validClass.password = passwordValid ? 'valid': 'invalid';
                break;
            case 'confirmPassword':
                confirmPasswordValid = this.state.confirmPassword === this.state.password;
                fieldValidationErrors.confirmPassword = confirmPasswordValid ? '': ' Password does not match';
                validClass.confirmPasswordValid = confirmPasswordValid ? 'valid': 'invalid';
                break;
            default:
                break;
        }
        this.setState({
                formErrors: fieldValidationErrors,
                emailValid: emailValid,
                passwordValid: passwordValid,
                confirmPasswordValid: confirmPasswordValid,
                validClass: validClass
            },
            this.validateForm);
    };

    validateForm() {
        this.setState({formValid: this.state.emailValid  &&
                this.state.passwordValid && this.state.confirmPasswordValid});
    };

    handleSubmit = (event) => {
        event.preventDefault();

        fetch('http://localhost:8000/rest-auth/registration/',
            {
                headers:{
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                mode: 'cors',
                credentials: 'include',
                body: JSON.stringify({email:this.state.email,
                password1:this.state.password,password2:this.state.confirmPassword})
            }
        )
            .then(response => response.json())
            .then(response => {
                if (response['key']) {
                    this.setState({message: 'You are successfully sign up.'})
                    this.setState({success:true});
                    this.changePopupState();
                }else{
                    this.setState({message: 'Something gone wrong. Try one more time.'})
                    this.changePopupState();
                }
            })
    };


    render () {
        return (
            <div className="signupPage">
                 {this.state.showPopup ? <SignPopup success={this.state.success}
                                                   message={this.state.message}
                                                   showPopup={this.changePopupState}/> : ''}

                <form className="FormSign" onSubmit = {this.handleSubmit}>

                    <div>
                        Email<span style={{color : 'red'}}>*</span>
                        <input className={this.state.validClass.email}
                               type="email" name="email"
                               placeholder="Email"
                               value={this.state.email}
                               onChange={this.handleUserInput}
                        />
                        <div className="formErrors">{this.state.formErrors.email}</div>
                    </div>
                    <div>
                        Password (6-50 symbols, big letter, small letter, number, @#$%)<span style={{color : 'red'}}>*</span>
                        <input className={this.state.validClass.password}
                            type="password" name="password"
                               placeholder="Password"
                               value={this.state.password}
                               onChange={this.handleUserInput}
                        />
                        <div className="formErrors">{this.state.formErrors.password}</div>
                    </div>

                    <div>
                        Confirm Password<span style={{color : 'red'}}>*</span>
                        <input className={this.state.validClass.confirmPassword}
                            type="password" name="confirmPassword"
                               placeholder="Password"
                               value={this.state.confirmPassword}
                               onChange={this.handleUserInput}
                        />
                        <div className="formErrors">{this.state.formErrors.confirmPassword}</div>
                    </div>

                    <br/>
                    <button type="submit" disabled={!this.state.formValid} className="waves-effect waves-light btn btn_color">Sign up</button>
                </form>
            </div>
        )
    }

}

export default SignUp;