import React from 'react';
import { Redirect } from 'react-router-dom';
import 'materialize-css';
import './styles.css';

//SignPopup  component for status of sign in, sign up operations
class SignPopup extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            redirect: false,
        }
        this.changeState = this.changeState.bind(this);
    };

    changeState(){
        this.setState({redirect:true});
    }

    render() {
        let redirect = this.state.redirect;
        if (redirect) {
            return <Redirect to='/' />
        }
        return (
            <div className="popup">
                <div className='popup_inner'>
                    <p>{this.props.message}</p>
                    { this.props.success ?
                        <button className='btn btn_color' onClick={this.changeState}>
                            Ok!
                        </button>
                        :
                        <button className='btn btn_color' onClick={this.props.showPopup}>
                            Ok!
                        </button>
                    }
                </div>
            </div>
        );
    }

}

export default SignPopup;