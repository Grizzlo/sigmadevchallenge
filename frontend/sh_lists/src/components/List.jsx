import React from 'react';
import { Link } from 'react-router-dom';
import 'materialize-css';
import './styles.css';
import Popup from "./Popup";

//List component for render list of all Shopping lists of some user
class List extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            link_template:'/list/',
            show:false,
        }
        this.onClickDelete = this.onClickDelete.bind(this);
        this.onDelete = this.onDelete.bind(this);
        this.showPopup = this.showPopup.bind(this)
    };

    onDelete(){
        this.props.confirmDelete(this.state.delete);
        this.showPopup();
        this.setState({delete:''});
    }

    showPopup() {
        this.setState(prevState =>({
      showPopup: !prevState.showPopup
    }));
    }

    onClickDelete(pk){
        this.setState({delete:pk});
        this.showPopup();
    }

    render() {
        let list;
        if (this.props.fake === true){
            list = <React.Fragment>
              <ul className="collection custom_list">
                  {this.props.list.map((name,i) => {
                      return (
                          <li className="collection-item custom_color">
                              <div key={i}>
                                  <a href="#!" className="secondary-content custom-left">
                                      <i className='small material-icons'>shopping_cart</i>
                                  </a>
                                  {name}
                                  <a href="#!" className="secondary-content custom-right">
                                      <i className='small material-icons'>clear</i>
                                  </a>
                              </div>
                          </li>
                      )
                  })
                  };
              </ul>
          </React.Fragment>
        }
        else {
             list = <React.Fragment>
                  <ul className="collection custom_list">
                      {this.props.list.results.map((item) => {
                          return (
                              <li className="collection-item custom_color">
                                  <div key={item.pk}>

                                      <a href={this.state.link_template+item.pk} className="secondary-content custom-left">
                                          <i className='small material-icons'>shopping_cart</i>
                                      </a>
                                      {item.done ?
                                          <a href={this.state.link_template + item.pk}  style={{textDecoration: 'line-through'}}>
                                              {item.title}
                                          </a>
                                          :
                                          <a href={this.state.link_template + item.pk}>
                                              {item.title}
                                          </a>
                                      }
                                      <div  className="secondary-content custom-right" >
                                          <i className='small material-icons' onClick={()=>this.onClickDelete(item.pk)} >
                                              clear
                                          </i>
                                      </div>
                                  </div>
                              </li>
                          )
                      })
                      }
                  </ul>
              </React.Fragment>
        }
            return (
                <React.Fragment>
                    {list}
                    {this.state.showPopup ? <Popup
                        showPopup={this.showPopup}
                        confirmDelete={this.onDelete}
                    /> : ''}
                </React.Fragment>
            );
        }
}
export default List;