from .views import ShoppingListsView, ShoppingListsCreateView, ShoppingListDetailView
from django.urls import path


# urls for list endpoints: all - call all list of some user, list/create create list,
# list/detail/<int:pk> for editting list
urlpatterns = [
    path('all', ShoppingListsView.as_view()),
    path('list/create', ShoppingListsCreateView.as_view()),
    path('list/detail/<int:pk>', ShoppingListDetailView.as_view()),

]
