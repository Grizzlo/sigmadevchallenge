from django.urls import path, include

# urls for app: rest-auth for auth and registration APIes endpoint(django-rest-auth library) and
# own path (api/v1/shopping_lists) shopping list API endpoints
urlpatterns = [
    path('rest-auth/', include('rest_auth.urls')),
    path(r'rest-auth/registration/', include('rest_auth.registration.urls')),
    path('api/v1/shopping_lists/', include('lists.urls')),
]
