from django.db import models
from django.conf import settings
# Create your models here.


# Model for shopping list. save() function check is all items done(if true - list.done = True) and save list
class ShopList(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title = models.CharField(max_length=50, null=False, blank=False)
    done = models.BooleanField(blank=False, default=False)

    def save(self, *args, **kwargs):
        items = ShopItem.objects.all().filter(list=self.pk)
        is_done = True
        for item in items:
            if item.done == False:
                is_done = False
                break
        self.done = is_done
        super(ShopList, self).save(*args, **kwargs)


# Model for shopping list. save() function check is all items done(if true - list.done = True) and save items
class ShopItem(models.Model):
    title = models.CharField(max_length=50, null=True, blank=True)
    list = models.ForeignKey(ShopList, related_name='items', on_delete=models.CASCADE, null=True)
    done = models.BooleanField(blank=False, default=False)

    def save(self, *args, **kwargs):
        super(ShopItem, self).save(*args, **kwargs)
        items = ShopItem.objects.all().filter(list=self.list.pk)
        list = ShopList.objects.all().get(pk=self.list.pk)
        is_done = True
        for item in items:
            if item.done == False:
                is_done = False
                break
        list.done = is_done
        list.save()
