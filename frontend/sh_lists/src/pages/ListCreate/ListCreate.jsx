import React from 'react';
import {Redirect} from 'react-router-dom';
import 'materialize-css';
import './listcreate.css';


//List create component for creating new shopping list
class ListCreate extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            items:[],
            new_item:{
                title:'',
                done:false,
            },
            title:'',
            redirect:false,
            isValid:false,
            errorMessage:'Field have length more then 30 or empty.',
            showError:false,
            addValid:false,
            showTitleError:false,
        }
        this.addItem = this.addItem.bind(this)
        this.newItemHandleChange = this.newItemHandleChange.bind(this);
        this.titleHandleChange = this.titleHandleChange.bind(this);
        this.cancelHandle = this.cancelHandle.bind(this);
        this.submitHandle = this.submitHandle.bind(this);
        this.deleteItemHandle = this.deleteItemHandle.bind(this);
        this.checkboxHandle = this.checkboxHandle.bind(this);
        this.checkboxItemHandle = this.checkboxItemHandle.bind(this);
    };


    checkboxHandle(e){
        this.state.new_item.done = e.target.checked;
        this.setState({new_item:this.state.new_item });
    }
    addItem(){
        this.setState({
            items:[...this.state.items, this.state.new_item],
            new_item:{
                title:'',
                done:false,
            },
            addValid:false,
        });
    };


    checkboxItemHandle(e, index) {
        this.state.items[index].done = e.target.checked;
        this.setState({items: this.state.items});
    }

    newItemHandleChange(e){
        if ((e.target.value.length > 0)&(e.target.value.length) <= 30) {
            this.setState({
                new_item: {
                    title: e.target.value,
                    done: false,
                },
                showError:false,
                addValid:true
            });

        } else {
            this.setState({
                new_item: {
                    title: e.target.value,
                    done: false,
                },
                showError:true,
                addValid:false
            });
        }
    }

    titleHandleChange(e){
      if ((e.target.value.length > 0)&(e.target.value.length) <= 30) {
            this.setState({
                title:e.target.value,
                showTitleError:false,
                isValid:true
            });

        } else {
            this.setState({
                title:e.target.value,
                showTitleError:true,
                isValid:false
            });
        }
    }

    cancelHandle(){
      this.setState({redirect: true});
    }

    submitHandle(){
        let obj = {};
        obj.title = this.state.title;
        obj.done = false;
        obj.items = this.state.items;
        fetch('http://localhost:8000/api/v1/shopping_lists/list/create',
        {
          headers:{
              'Content-Type': 'application/json',
              'Authorization': 'Token '+localStorage.getItem("token"),
          },
          method: 'POST',
          mode: 'cors',
          credentials: 'include',
          body: JSON.stringify(obj),
        })
        .then(response => response.json())
        .then(response => {
          if (response){
             this.cancelHandle();
          }
        });
    }


    deleteItemHandle(index){
        this.state.items.splice(index,1);
        this.setState({items: this.state.items});
    }


    render() {
        const redirect = this.state.redirect;
        if (redirect) {
            return <Redirect to='/'/>;
        }
        return (
            <div className="lcreate_wrapper">
                <h1>Creating new shopping list</h1>
                <div className="form_wrapper">
                    <div>
                        <input placeholder='Enter List Title'
                               className='title_input custom_input'
                               value={this.state.title}
                               onChange={(e)=>this.titleHandleChange(e)}>
                        </input>
                        {this.state.showTitleError ?
                            <p style={{color:'red'}}> {this.state.errorMessage} </p> : ''
                        }
                    </div>
                    <div>
                        {this.state.items.map((item,index) =>{
                            return(
                                <div key={index} className='item'>
                                    {item.done ?
                                        <p style={{textDecoration:'line-through'}} className='custom_input'>
                                            {item.title}
                                        </p>
                                    :
                                        <p className='custom_input'>{item.title}</p>
                                    }
                                    <p>
                                        <label>
                                            <input type="checkbox"
                                             checked={item.done}
                                             className='custom_input'
                                             onChange={(e)=>this.checkboxItemHandle(e,index)}/>
                                            <span> </span>
                                        </label>
                                    </p>
                                    <i className="material-icons small" onClick={()=>this.deleteItemHandle(index)}>
                                        clear
                                    </i>
                                </div>
                            )
                            })
                        }
                    </div>
                    <div className='new_item'>
                        {this.state.new_item.done ?
                            <input placeholder='Enter Item Name'
                                   className='custom_input'
                                   style={{textDecoration: 'line-through'}}
                                   value={this.state.new_item.title}
                                   onChange={(e) => this.newItemHandleChange(e)}>
                            </input>
                            :
                            <input placeholder='Enter Item Name'
                                   className='custom_input'
                                   value={this.state.new_item.title}
                                   onChange={(e) => this.newItemHandleChange(e)}>
                            </input>
                        }
                        <p>
                            <label>
                                <input type="checkbox"
                                 checked={this.state.new_item.done}
                                 onChange={this.checkboxHandle}/>
                                <span> </span>
                            </label>
                        </p>
                        <button onClick={this.addItem} disabled={!this.state.addValid} className='btn btn_color'>Add</button>
                    </div>
                </div>
                <div className='control_button'>
                    <button onClick={this.cancelHandle} className='btn btn_color'>Cancel</button>
                    <button onClick={this.submitHandle} disabled={!this.state.isValid} className='btn btn_color'>Save</button>
                </div>
                <div>
                    {this.state.showError ?
                         <p style={{color:'red'}}> {this.state.errorMessage} </p> : ''
                    }
                </div>
            </div>

        );
    }
}

export default ListCreate;