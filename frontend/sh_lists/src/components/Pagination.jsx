import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import './styles.css';

//Component for simple Pagination
class Paginator extends React.Component{
    constructor(props){
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(item){
        this.props.changePage(item);
    }
    render() {
        let number;
        if (this.props.count%10 != 0) {
            number =Math.trunc(this.props.count/10) + 1;
        }
        else {
            number =Math.trunc(this.props.count/10);
        }
        let list = [];
        for (let i = 1; i <= number; i++) {
            list.push(i);
        };
        let pags = list.map((item)=> {
            return(
                 <li className="waves-effect custom_pagination" onClick={()=>this.handleClick(item)}>{item}</li>
            );
            });
        return(
            <ul className="pagination">
                {pags}
            </ul>
        )
    }
}

export default Paginator;