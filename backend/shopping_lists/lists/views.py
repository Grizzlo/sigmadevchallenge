from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import IsAuthenticated
from .models import ShopList
from .serializers import ShopListDetailSerializer, ShopListSerializer


# API view for creating shopping list
class ShoppingListsCreateView(generics.CreateAPIView):
    serializer_class = ShopListDetailSerializer
    permission_classes = [IsAuthenticated]
    queryset = ShopList.objects.all()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


# API view for viewing  all shopping lists of some user
class ShoppingListsView(generics.ListAPIView):
    serializer_class = ShopListSerializer
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return ShopList.objects.all().filter(user=self.request.user).order_by('-pk')


# API view for update, delete, get shopping list
class ShoppingListDetailView(generics.RetrieveUpdateDestroyAPIView):

    serializer_class = ShopListDetailSerializer
    permission_classes = [IsAuthenticated]
    queryset = ShopList.objects.all()

    def get_object(self):
        return ShopList.objects.all().get(pk=self.kwargs.get('pk'), user=self.request.user)

