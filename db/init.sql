CREATE USER sh_list_user WITH PASSWORD '8J/Gd<kfJZ!Dr<zP2M';
CREATE DATABASE sh_list_db;

GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO sh_list_user;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO sh_list_user;
GRANT ALL PRIVILEGES ON DATABASE sh_list_db TO sh_list_user;