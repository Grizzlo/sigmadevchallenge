from rest_framework import serializers
from .models import ShopList, ShopItem


# Model based Serializer for all shopping lists
class ShopListSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShopList
        fields = ['pk', 'title', 'user', 'done']
        read_only_fields = ['user', 'done']


# Model based Serializer for Item
class ShopItemSerializer(serializers.ModelSerializer):
    id = serializers.ModelField(model_field=ShopItem()._meta.get_field('id'), required=False)

    class Meta:
        model = ShopItem
        fields = '__all__'
        read_only_fields = ['id']


# Model based Serializer for editing shopping list and item in it
class ShopListDetailSerializer(serializers.ModelSerializer):
    items = ShopItemSerializer(many=True)

    class Meta:
        model = ShopList
        fields = ['pk', 'title', 'user', 'done', 'items']
        read_only_fields = ['user', 'pk', 'done']

    def create(self, validated_data):
        items_data = validated_data.pop('items')
        shoplist = ShopList.objects.create(**validated_data)
        for item_data in items_data:
            ShopItem.objects.create(list=shoplist, **item_data)
        return shoplist

    def update(self, instance, validated_data):
        instance.title = validated_data.get('title', instance.title)
        instance.pk = validated_data.get('pk', instance.pk)
        instance.save()
        items_data = list((instance.items).all().values_list('id', flat=True))
        items = validated_data.get('items')
        for item in items:
            item_id = item.get('id', None)
            if item_id:
                items_data.remove(item_id)
                list_item = ShopItem.objects.get(id=item_id, list=instance)
                list_item.title = item.get('title', list_item.title)
                list_item.done = item.get('done', list_item.done)
                list_item.save()
            else:
                ShopItem.objects.create(list=instance, **item)

        for item in items_data:
            ShopItem.objects.filter(id=item).delete()

        return instance
