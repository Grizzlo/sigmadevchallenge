import React from 'react';
import { Link, Redirect } from 'react-router-dom';
import './main.css';
import List from "../../components/List";
import Paginator from "../../components/Pagination";
import Popup from "../../components/Popup";

//Components for Main Page with Pagination
class Main extends React.Component {
  constructor(props) {
      super(props);
      this.state = {
          lists:{
              count:'',
              next:'',
              previous:'',
              results: [],
          },
          fakeLists:[
              'Ashan shopping',
              'Metro shopping',
              'Rozetka shopping',
              'Evo shopping',
              'Epicentr shopping',
              'ATB shopping',
          ],
          redirect:false,
          redirect2:false,
          currentPage:1,
          perPage:10,
          showPopup:false,
      };
      this.newList = this.newList.bind(this);
      this.changePage = this.changePage.bind(this);
      this.confirmDelete = this.confirmDelete.bind(this);

  };


  changePage(number){
      this.getLists(number);
  }

  newList(){
      this.setState({redirect:true});
  }

  confirmDelete(pk) {
        fetch('http://127.0.0.1:8000/api/v1/shopping_lists/list/detail/'+pk,
        {
          headers:{
              'Content-Type': 'application/json',
              'Authorization': 'Token '+localStorage.getItem("token"),
          },
          method: 'DELETE',
          mode: 'cors',
          credentials: 'include',
        })
        .then(response => {
          if (response){

          }
        });
        setTimeout(function (){
            this.componentWillMount()
        }.bind(this), 100);
  }

  getLists(number){
      fetch('http://127.0.0.1:8000/api/v1/shopping_lists/all?page='+number,
        {
          headers:{
              'Content-Type': 'application/json',
              'Authorization': 'Token '+localStorage.getItem("token"),
          },
          method: 'GET',
          mode: 'cors',
          credentials: 'include',
        })
        .then(response => {
          if (response.ok) {
            response.json().then(json => {
             this.setState({lists:json});
            });
          }
        });
      this.setState({currentPage:number});
      this.setState({showPopup:false});
  }

  componentWillMount() {
    this.getLists(this.state.currentPage);
  }

    render() {
      let renderBlock;
      let pag = this.state.lists.count > 10 ? true : false;
      if (this.state.redirect){
          return <Redirect to='/list/create'/>;
      }
      if (this.props.user){
          renderBlock = <React.Fragment>
                        <button onClick={this.newList} className='btn btn_color'>Add</button>
                            <List list={this.state.lists}
                                  fake={false}
                                  confirmDelete = {this.confirmDelete}
                            />
                            {pag ? (<Paginator currentPage={this.state.currentPage}
                            count={this.state.lists.count}
                            changePage={this.changePage}/>):('')}
                        </React.Fragment>
      }
      else {
          renderBlock = <React.Fragment>
                          <h4>You are not authorized. Please <Link to="/signin">Sign in</Link> or <Link to="/signup">Sign up!</Link> </h4>
                          <h5>App for creating your own shopping list</h5>
                          <List list={this.state.fakeLists} fake={true}/>
                        </React.Fragment>
      }
      return (
         <div className="main_wrapper">
             <h1>My shopping lists</h1>
             {renderBlock}
         </div>
      );
  }

}

export default Main;