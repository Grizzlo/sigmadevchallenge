import React from 'react';
import { Link } from 'react-router-dom';
import 'materialize-css';
import './navbar.css';

//Components for App Navbar: consist of auth info and link to main page

const Sign = () =>{
    return(
        <React.Fragment>
             <li><Link  to="/signup">Sign up</Link></li>
             <li><Link  to="/signin">Sign in</Link></li>
        </React.Fragment>
    )
};


const User = (props) =>{
    function handleClick(){
        fetch('http://localhost:8000/rest-auth/logout/',
            {
                headers:{
                    'Content-Type': 'application/json',
                    'Authorization': 'Token '+localStorage.getItem("token"),
                },
                method: 'POST',
                mode: 'cors',
                credentials: 'include',
            }
        )
        .then(response => response.json())
        .then(response => {
            localStorage.removeItem('token');
            props.handleClick('');
        }
        );
    }


    return(
            <React.Fragment>
                <li><span className="custom_wrapper">{props.user.email}</span></li>
                <li><button  onClick={handleClick} className="waves-effect waves-light btn btn_color" >Log out</button></li>
            </React.Fragment>
    )
};

const Navbar = (props) => {
    let header;
    if (props.user) {
        header = <User user={props.user}
                       handleClick={props.handleClick}/>
    }else{
        header = <Sign/>
    }

    return (
        <nav className="custom_wrapper purple darken-4">
            <div className="nav-wrapper">
                <Link  to="/" className="brand-logo">Shopping Lists</Link>
                <ul id="nav-mobile" className="right hide-on-med-and-down">
                    {header}
                </ul>
            </div>
        </nav>
    )
};

export default Navbar;
