import React, { Component } from 'react';
import {Redirect} from "react-router-dom";
import SignPopup from "../../components/SignPopup";
import './signin.css'


//Component for Signin Page with field validators
class SignIn extends Component {
    constructor (props) {
        super(props);
        this.state = {
            email: '',
            password: '',
            formErrors: {email: '', password: ''},
            emailValid: false,
            passwordValid: false,
            formValid: false,
            showPopup: false,
            isUser: '',
            success:false,
            validClass: {email: 'valid', password: 'valid'}
        };
        this.changePopupState = this.changePopupState.bind(this);
    };

    handleUserInput = (e) => {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({[name]: value},
            () => {this.validateField(name, value)}
        );
    };

    validateField(fieldName, value) {
        let fieldValidationErrors = this.state.formErrors;
        let emailValid = this.state.emailValid;
        let passwordValid = this.state.passwordValid;
        let validClass = this.state.validClass;

        switch(fieldName) {
            case 'email':
                emailValid = value.match(/^(?=.{10,50}$)([a-z0-9_-]+.)*[a-z0-9_-]+@[a-z0-9_-]+(.[a-z0-9_-]+)*.[a-z]{2,6}$/i);
                fieldValidationErrors.email = emailValid ? '' : ' is invalid';
                validClass.email = emailValid ? 'valid': 'invalid';
                break;
            case 'password':
                passwordValid = value.match(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,50}$/);
                passwordValid = true
                fieldValidationErrors.password = passwordValid ? '': ' is too short';
                validClass.password = passwordValid ? 'valid': 'invalid';
                break;
            default:
                break;
        }
        this.setState({
                formErrors: fieldValidationErrors,
                emailValid: emailValid,
                passwordValid: passwordValid,
                validClass: validClass
            },
            this.validateForm);
    };

    validateForm() {
        this.setState({formValid: this.state.emailValid && this.state.passwordValid});
    };

    handleSubmit = (event) => {
        event.preventDefault();

        fetch('http://localhost:8000/rest-auth/login/',
            {
                headers:{
                    'Content-Type': 'application/json'
                },
                method: 'POST',
                mode: 'cors',
                credentials: 'include',
                body: JSON.stringify({username:"", email:this.state.email, password:this.state.password})
            }
        )
            .then(response => response.json())
            .then(response => {
                if (response['key']) {
                    localStorage.setItem("token", response['key']);
                    this.props.handleClick(response['key']);
                    this.setState({isUser: 'You are successfully sign in.'})
                    this.setState({success:true});
                    this.changePopupState();
                }else {
                    this.setState({isUser: 'Wrong password or email. Try one more time.'})
                    this.changePopupState();
                }
            })
    };

    changePopupState(){
        this.setState(prevState =>({
      showPopup: !prevState.showPopup
      }))
    }

    render () {

        return (
            <div className="signindiv">
                {this.state.showPopup ? <SignPopup success={this.state.success}
                                                   message={this.state.isUser}
                                                   showPopup={this.changePopupState}/> : ''}
                <form className="FormSign" onSubmit = {this.handleSubmit}>
                    <div>
                        <div>Email</div>
                        <input className={this.state.validClass.email}
                            type="email" name="email"
                            placeholder="Email"
                            value={this.state.email}
                            onChange={this.handleUserInput}
                        />
                        <div className="formErrors">{this.state.formErrors.email}</div>
                    </div>

                    <div>
                        <div>Password</div>
                        <input className={this.state.validClass.password}
                            type="password" name="password"
                            placeholder="Password"
                            value={this.state.password}
                            onChange={this.handleUserInput}
                        />
                        <div className="formErrors">{this.state.formErrors.password}</div>
                    </div>

                    <button type="submit" disabled={!this.state.formValid} className="waves-effect waves-light btn btn_color">Sign in</button>
                </form>
            </div>

        )
    }
}

export default SignIn;
