# Shopping List
App for managing your shopping lists.

## Requirements

1. Python 3.7/ Django 2.2
2. React 16.2
3. PostgeSQL 10.5+.

## Installation
#### Install database
To create database run ./db/inital.sql script in your postges.
#### Install frontend app
To install frontend dependency  run `npm install` in ./frontend/sh_lists/
#### Install backend app

To install backend dependency  run `pip install -r requirements.txt` in ./backend/
Then to apply our changes to database run `python manage.py migrate`
### Run apps

To run backend app write `python manage.py runserver` in terminal in ./backend/shopping_list/ folder.

To run frontend app write `npm start ` in terminal in .frontend/sh_lists/ folder.

To open app  - open localhost:3000 in browser.