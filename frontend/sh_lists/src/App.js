import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Navbar from'./pages/Navbar/Navbar';
import SignIn from'./pages/Signin/Signin';
import SignUp from'./pages/Signup/Signup';
import Main from './pages/Main/Main';
import ListDetail from './pages/ListDetail/ListDetail';
import ListCreate from "./pages/ListCreate/ListCreate";

//App file. have Route for all page
class App extends Component {
   constructor(props){
    super(props);
    this.state = {
      token: localStorage.getItem("token"),
      user:'',
    };
    this.handleClick = this.handleClick.bind(this);
    this.getUser = this.getUser.bind(this);
  }

  handleClick(token){
      this.setState({user:''});
      this.getUser(token);
  };
  getUser(token){
          fetch('http://localhost:8000/rest-auth/user/',
    {
      headers:{
          'Content-Type': 'application/json',
          'Authorization': 'Token '+token,
      },
      method: 'GET',
      mode: 'cors',
      credentials: 'include',
    })
    .then(response => response.json())
    .then(response => {
      if (response['pk']){
          this.setState({user: response});
      }
    });
  }
  componentWillMount(){
    this.getUser(this.state.token);
  }

  render() {
    return (
        <BrowserRouter>
            <div className="app">
              <Navbar user={this.state.user}
                      handleClick={this.handleClick}/>
              <Switch>
                <Route exact path="/" render={() => <Main user={this.state.user}/>}/>
                <Route path="/list/create" component={ListCreate}/>
                <Route path="/list/:listId" render={() => <ListDetail user={this.state.user}/>}/>
                <Route path="/signin" render={() => <SignIn handleClick={this.handleClick}/>} />
                <Route path="/signup" render={() => <SignUp handleClick={this.handleClick}/>} />
                <Route component={Error} />
              </Switch>
              {/* <Footer /> */}
            </div>
        </BrowserRouter>
    );
  }
}

export default App;
