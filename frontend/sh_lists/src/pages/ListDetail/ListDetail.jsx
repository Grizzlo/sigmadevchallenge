import React from 'react';
import { withRouter, Redirect } from 'react-router-dom';
import 'materialize-css';

//ListDetail component for operations with  shopping list
class ListDetail extends React.Component {
    constructor(props){
        super(props);
        this.state ={
            items:[],
            new_item:{
                title:'',
                done:false,
            },
            current_items:[],
            title:'',
            done:'',
            redirect:false,
            errorMessage:'Field have length more then 30 or empty.',
            showError:false,
            addValid:false,
        }
        this.addItem = this.addItem.bind(this);
        this.newItemHandleChange = this.newItemHandleChange.bind(this);
        this.cancelHandle = this.cancelHandle.bind(this);
        this.submitHandle = this.submitHandle.bind(this);
        this.deleteItemHandle = this.deleteItemHandle.bind(this);
        this.deleteCurrentItemHandle = this.deleteCurrentItemHandle.bind(this);
        this.checkboxHandle = this.checkboxHandle.bind(this);
        this.checkboxItemHandle = this.checkboxItemHandle.bind(this);
        this.checkboxCurrentItemHandle = this.checkboxCurrentItemHandle.bind(this);
    };

    checkboxHandle(e){
        this.state.new_item.done = e.target.checked;
        this.setState({new_item:this.state.new_item });
    }
    addItem(){
        this.setState({
            items:[...this.state.items, this.state.new_item],
            new_item:{
                title:'',
                done:false,
                addValid:false,
            },
        });
    };


    checkboxItemHandle(e, index) {
        this.state.items[index].done = e.target.checked;
        this.setState({items: this.state.items});
    }
    checkboxCurrentItemHandle(e, index) {
        this.state.current_items[index].done = e.target.checked;
        this.setState({current_items: this.state.current_items});
    }

    newItemHandleChange(e){
        if ((e.target.value.length > 0)&(e.target.value.length) <= 30) {
            this.setState({
                new_item: {
                    title: e.target.value,
                    done: false,
                },
                showError:false,
                addValid:true
            });

        } else {
            this.setState({
                new_item: {
                    title: e.target.value,
                    done: false,
                },
                showError:true,
                addValid:false
            });
        }
    }

    cancelHandle(){
      this.setState({redirect: true});
    }

    submitHandle(){
        let obj = {};
        obj.title = this.state.title;
        obj.items = this.state.items.concat(this.state.current_items);
        fetch('http://localhost:8000/api/v1/shopping_lists/list/detail/'+this.props.match.params.listId,
        {
          headers:{
              'Content-Type': 'application/json',
              'Authorization': 'Token '+localStorage.getItem("token"),
          },
          method: 'PUT',
          mode: 'cors',
          credentials: 'include',
          body: JSON.stringify(obj),
        })
        .then(response => {
          if (response.ok) {
              this.cancelHandle();
          }
        });
    }

    componentWillMount() {
        fetch('http://localhost:8000/api/v1/shopping_lists/list/detail/'+this.props.match.params.listId,
        {
          headers:{
              'Content-Type': 'application/json',
              'Authorization': 'Token '+localStorage.getItem("token"),
          },
          method: 'GET',
          mode: 'cors',
          credentials: 'include',
        })
        .then(response => {
          if (response.ok) {
            response.json().then(json => {
                 this.setState({current_items:json.items});
                 this.setState({title:json.title});
                 this.setState({done:json.done});
            });
          }
        });
    }

    deleteItemHandle(index){
        this.state.items.splice(index,1);
        this.setState({items: this.state.items});
    }

    deleteCurrentItemHandle(index){
        this.state.current_items.splice(index,1);
        this.setState({current_items: this.state.current_items});
    }


    render() {
        const redirect = this.state.redirect;

        if (redirect) {
            return <Redirect to='/'/>;
        }

        return (
            <div className="lcreate_wrapper">
                <h1>Editing shopping list</h1>
                <div className="form_wrapper">
                    <div>
                        <h3>
                            {this.state.title}
                        </h3>
                    </div>
                    <div>
                        {this.state.current_items.map((item,index) =>{
                            return(
                                <div key={index} className='item'>
                                    {item.done ?
                                        <p style={{textDecoration:'line-through'}} className='custom_input'>
                                            {item.title}
                                        </p>
                                    :
                                        <p className='custom_input'>{item.title}</p>
                                    }
                                    <p>
                                        <label>
                                            <input type="checkbox"
                                             checked={item.done}
                                             className='custom_input'
                                             onChange={(e)=>this.checkboxCurrentItemHandle(e,index)}/>
                                            <span> </span>
                                        </label>
                                    </p>
                                    <i className="material-icons small" onClick={()=>this.deleteCurrentItemHandle(index)}>
                                        clear
                                    </i>
                                </div>
                            )
                            })
                        }
                    </div>
                    <div>
                        {this.state.items.map((item,index) =>{
                            return(
                                <div key={index} className='item'>
                                    {item.done ?
                                        <p style={{textDecoration:'line-through'}} className='custom_input'>
                                            {item.title}
                                        </p>
                                    :
                                        <p className='custom_input'>{item.title}</p>
                                    }
                                    <p>
                                        <label>
                                            <input type="checkbox"
                                             checked={item.done}
                                             className='custom_input'
                                             onChange={(e)=>this.checkboxItemHandle(e,index)}/>
                                            <span> </span>
                                        </label>
                                    </p>
                                    <i className="material-icons small" onClick={()=>this.deleteItemHandle(index)}>
                                        clear
                                    </i>
                                </div>
                            )
                            })
                        }
                    </div>
                    <div className='new_item'>
                         {this.state.new_item.done ?
                            <input placeholder='Enter Item Name'
                                   className='custom_input'
                                   style={{textDecoration: 'line-through'}}
                                   value={this.state.new_item.title}
                                   onChange={(e) => this.newItemHandleChange(e)}>
                            </input>
                            :
                            <input placeholder='Enter Item Name'
                                   className='custom_input'
                                   value={this.state.new_item.title}
                                   onChange={(e) => this.newItemHandleChange(e)}>
                            </input>
                        }
                        <p>
                            <label>
                                <input type="checkbox"
                                 checked={this.state.new_item.done}
                                 onChange={this.checkboxHandle}/>
                                <span> </span>
                            </label>
                        </p>
                        <button onClick={this.addItem} disabled={!this.state.addValid} className='btn btn_color'>Add</button>
                    </div>
                </div>
                <div className='control_button'>
                    <button onClick={this.cancelHandle} className='btn btn_color'>Cancel</button>
                    <button onClick={this.submitHandle} className='btn btn_color'>Save</button>
                </div>
                <div>
                    {this.state.showError ?
                         <p style={{color:'red'}}> {this.state.errorMessage} </p> : ''
                    }
                </div>
            </div>

        );
    };

}


export default withRouter(ListDetail);