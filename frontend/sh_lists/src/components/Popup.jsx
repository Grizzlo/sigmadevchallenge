import React from 'react';
import { Link } from 'react-router-dom';
import 'materialize-css';
import './styles.css';

//Popup component for confirm deleting of list
class Popup extends React.Component {
    constructor(props) {
        super(props);

    };


    render() {
        return (
            <div className="popup">
                <div className='popup_inner'>
                    <p>Do you really want delete this list?</p>
                    <button className='btn btn_color' onClick={this.props.showPopup}>Cancel</button>
                    <button className='btn btn_color' onClick={this.props.confirmDelete}>
                        Ok!
                    </button>
                </div>
            </div>
        );
    }

}

export default Popup;